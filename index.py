from flask import Flask, render_template, redirect, request
import utils.capture as capture
import utils.start_gpio as start_gpio
import service

app = Flask(__name__)
stalkers = dict() # in memory 
pid = 0
fname = 'log_net'

@app.route('/')
def index():
    global fname
    global stalkers
    global pid
    is_reload = request.args.get('is_reload')
    root_url = request.url_root
    developer_url = 'http://127.0.0.1:5000/'
    is_from_raspi = False
    is_exist_stalker = False

    if root_url == developer_url:
        is_from_raspi = True
    print(is_reload)
    print(stalkers)
    if is_from_raspi:
        if is_reload:
            with open(fname, 'r') as f:
                content = f.read()
            ls = content.split('\n')
            new_stalkers = {}

            for i in ls:
                if 'Probe' in i:
                    parsed = i.split()
                    new_stalkers[parsed[1]] = parsed[0]
            for i in stalkers:
                if i in new_stalkers:
                    stalkers[i] = (new_stalkers[i], stalkers[i][1]+1)
                    if stalkers[i][1] >= 3:
                        is_exist_stalker = True
                else:
                    stalkers[i] = (0, 0)

            for i in new_stalkers:
                if i not in stalkers:
                    stalkers[i] = (new_stalkers[i], 1)

            if is_exist_stalker:
                start_gpio.main()
            else:
                pass
            capture.reload_capture(pid, 'wlan1mon')
        else:
            pid = capture.start_capture('wlan1mon')

    formatted = []
    for i in stalkers:
        formatted.append((i, stalkers[i][1]*10, str(-int(stalkers[i][0]))))
    formatted.sort(key=lambda stalker: (stalker[1], stalker[2]), reverse=True)

    return render_template('index.html', stalkers=formatted[:10])

@app.route('/reset')
def reset():
    stalkers = dict()
    capture.airmon_stop('wlan1')
    capture.airmon_start('wlan1')
    return redirect("/")
