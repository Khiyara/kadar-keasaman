import subprocess
import datetime


def reload_capture(pid, interface_name, fname="log_net"):
    subprocess.Popen(['sudo', 'kill', '-SIGKILL', str(pid)])
    with open(fname, 'w') as f:
        proc = subprocess.Popen(['sudo', 'tshark', '-i', interface_name, '-T', 'fields', '-e', 'wlan_radio.signal_dbm', '-e', 'wlan.sa', '-e', 'wlan'], stdout=f)
    return proc.pid


def start_capture(interface_name, fname='log_net'):
    with open(fname, 'w') as f:
        proc = subprocess.Popen(['sudo', 'tshark', '-i', interface_name, '-T', 'fields', '-e', 'wlan_radio.signal_dbm', '-e', 'wlan.sa', '-e', 'wlan'], stdout=f)
    return proc.pid


def airmon_start(interface_name):
    subprocess.Popen(['sudo', 'airmon-ng', 'start', interface_name])
 

def airmon_stop(interface_name):
    subprocess.Popen(['sudo', 'airmon-ng', 'stop', interface_name])


def main():
    interface_name = "wlan0mon"
    monitor_interface = "wlan0mon"
    airmon_start(interface_name)
    fpointer = capture(monitor_interface)
    while True:
        process_capture(capture_result)
        notif_user()

if __name__ == '__main__':
    main()
