#!/bin/bash

export FLASK_APP=index.py
export FLASK_DEBUG=1
flask run --host 0.0.0.0 &
export DISPLAY=:0
firefox-esr --new-window http://localhost:5000
